# video_player_vue

## Description
```
Component for Vue:

Video Player with castom control panel and playlist.
Save progress of watching a video, volum and video playback speed in localStore
```

## Project setup
```
npm install video_player_vue
```
## Usage

```HTML
<VideoPlayer :arr_videos="arr_videos"/>
```

```javascript
import VideoPlayer from 'video_player_vue'

// require style
import 'video_player_vue/dist/video_player_vue.css'

export default {
  components: {
    VideoPlayer
  },
  data: () => ({
      arr_videos:[
      	{
      		id:1,
      		name:'video1',
      		src:'./vid1.mp4',
      		time:294
      	},
        {	
        	id:2,
        	name:'video2',
        	src:'./vid2.mp4',
        	time:341
        }
      ]
  }),
}
```
```
add Bootstrap css in index.html
```
```HTML
<!-- require bootstrap style -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
```

## API

### video_player_vue

#### props 

+ `arr_videos` ***[ Array, default: [] ]*** (*optional*) 
:array of videos it should be ***Object*** (*required*)
  - `id` ***Number*** (*required*, *unique*) : to correctly maintain the progress of watching a video
  - `name` ***String*** (*required*) : will be displayed in the playlist
  - `src` ***String*** (*required*) : path or API link on video
  - `time` ***Number*** (*required*) : video time, in seconds
  - `from` ***Number*** (*optional*, default: 0 or variable from localStore) : time from which to start the video, in seconds
  - `poster` ***String, default:''*** (*optional*) : url to picture
- `width` ***Number, default: 60% from screen.width if(screen.width<600) screen.width*** (*optional*) : the width of the video player and playlist
- `color` ***String, default:'rgb(252, 231, 41)'*** (*optional*) : color of control buttons
- `list_height` ***Number, default:100%*** (*optional*) : height of playlist
- `list_color` ***String, default:'#a0a0a0'*** (*optional*) : playlist unit color
- `list_select_color` ***String, default:'#5755ff'*** (*optional*) : selected playlist unit color
- `list_text_color` ***String, default:'#ffffffe6'*** (*optional*) : playlist text color