class VideoInfo{
    _time = 8;
    _ready = false;
    _is_timer = false;
    _player_setting = {
        list_orientation:'col',
        volum:1,
        speed:1,
        auto_play:false
    }

    _timer(){
        if(!this._is_timer){
            this._is_timer = true;
            setTimeout(()=>{
                this._ready = true;
                this._is_timer = false;
            }, 1000*this._time);
        }
    }

    getPlayerSpeed(){
        return this._player_setting.speed;
    }

    getAutoPlay(){
        return this._player_setting.auto_play;
    }

    getPlayerSettings(){
        let player_setting = window.localStorage.getItem('player_setting');
        if(player_setting){
            this._player_setting = JSON.parse(player_setting);
        }
        return this._player_setting;
    }

    setListOrientation(side){
        this._player_setting.list_orientation = side;
        this.setPlayerSettings();
    }

    setSpeed(speed){
        this._player_setting.speed = speed;
        this.setPlayerSettings();
    }

    setVolum(volum){
        this._player_setting.volum = volum;
        this.setPlayerSettings();
    }

    setAutoPlay(val){
        this._player_setting.auto_play = val;
        this.setPlayerSettings();
    }

    setPlayerSettings(){
        window.localStorage.setItem('player_setting', JSON.stringify(this._player_setting));
    }

    getVideoTime(id){
        let time = window.localStorage.getItem('video'+id);
        if(!time){
            return 0;
        }
        return time;
    }

    setVideoTime(id,val){
        // if(!this._ready && val!==0){
        //     this._timer();
        //     return;
        // }
        // this._ready = false;
        
        window.localStorage.setItem('video'+id, val);
    }

    getCurrentVideo(){
        let id = window.localStorage.getItem('current_video');
        if(!id){
            return 0;
        }
        return id;
    }

    setCurrentVideo(id){
        window.localStorage.setItem('current_video', id);
    }
}

const videoInfo = new VideoInfo();

export default videoInfo;