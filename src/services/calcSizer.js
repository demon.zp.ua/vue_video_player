export default class calcSizer{
    static calcWidth(parent){
        let proc = 60;
        if(parent.control.list_orientation==='row'){
            proc = 44;
        }
        let width = Math.floor(screen.width*proc/100);
        
        if(parent.width){
            if(parent.width<screen.width && parent.control.list_orientation==='col'){
                return parent.width;
            }

            if(parent.width>width){
                return width;
            }
            //console.log(parent.width<screen.width);
            
        }

        //console.log('calcWidth = ', width);
        return width;
    }

    static calcSizeico(parent){
        let r = 800 - parent.control.width;
        const s = {
            size:0,
            width:0,
            height:0
        };
        if(r>0){ 
            s.size = parent.d_font_size - r*0.1;
            s.width = 22 - r*0.01;
            s.height = 22 - r*0.01;
        }else{
            r = Math.abs(r);
            s.size = parent.d_font_size + r*0.1;
            s.width = 22 + r*0.01;
            s.height = 22 + r*0.01;
        }
        //console.log('s = ', s);
        return s;
    }
}