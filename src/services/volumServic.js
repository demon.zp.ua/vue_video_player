import videoInfo from './videoInfo';

export default class volumServic{
    constructor(video,control){
        this.video = video;
        this.control = control;
    }

    onSound(){
        if(this.video.volume>0){
            this.video.volume = 0;
        }else{
            this.video.volume = this.control.befor_mute;
        }
    }

    onUpSound(){
        let vol = this.video.volume+0.1;
        if(vol>1){
            vol = 1;
        }
        this.onChangeSound(vol);
    }

    onDownSound(){
        let vol = this.video.volume-0.1;
        if(vol<0){
            vol = 0;
        }
        this.onChangeSound(vol);
    }

    onChangeSound(vol){
        videoInfo.setVolum(vol);
        this.video.volume = vol;
    }

    volumeChange(){
        if(this.video.volume==0){
            if(this.control.volum>0){
                //console.log('volum = ',this.control.volum);
                this.control.befor_mute = this.control.volum;
            }else{
                this.control.befor_mute = 1;
            }
        }else{
            this.control.befor_mute = this.video.volume;
        }
        this.control.volum = this.video.volume;
        //console.log('befor_mute = ',this.control.befor_mute);
    }
}