class FormData{
    getPreatyTime(time){
        time = Number(time);
        let hours = 0;
        let minuts = Math.floor(time/60);
        hours = Math.floor(minuts/60);
        if(hours>0){
            minuts = minuts - hours*60;
        }
        const sec = time - (hours*60*60+minuts*60);
        return `${this.getPreatyHours(hours)}${this.getDoubleNumberStr(minuts)}:${Math.trunc(this.getDoubleNumberStr(sec))}`;
    }

    getDoubleNumberStr(num){
        if(num<10){
            return `0${num}`;
        }
        return num;
    }

    getPreatyHours(hours){
        if(hours>0){
            return `${this.getDoubleNumberStr(hours)}:`
        }
        return '';
    }
}

const formData = new FormData();

export default formData;