export default class KeyBoardListener{
    constructor({ el, context }){
        this.el = el;
        this.context = context;
        this.events = {};
        this.el.addEventListener('keydown', this.onKeyUp);
    }

    addEvent(key,func){
        this.events[key] = func.bind(this.context);
    }

    onKeyUp = (e)=>{
        if(this.events.hasOwnProperty(e.code)){
            e.preventDefault();
            this.events[e.code]();
        }
    }
}